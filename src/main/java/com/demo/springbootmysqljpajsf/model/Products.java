package com.demo.springbootmysqljpajsf.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "products")
public class Products {
	
	@Id
	@NotNull
	@Size(max = 15)
	private String productCode;
	@NotNull
	@Size(max = 70)
	private String productName;
	@NotNull
	@Size(max = 50)
	private String productLine;
	@NotNull
	@Size(max = 10)
	private String productScale;
	@NotNull
	@Size(max = 50)
	private String productVendor;
	@NotNull
	@Lob
	@Column(columnDefinition = "TEXT")
	private String productDescription;
	@NotNull
	private short quantityInStock;
	@NotNull
	private BigDecimal buyPrice;
	@NotNull
	private BigDecimal MSRP;
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductLine() {
		return productLine;
	}
	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}
	public String getProductScale() {
		return productScale;
	}
	public void setProductScale(String productScale) {
		this.productScale = productScale;
	}
	public String getProductVendor() {
		return productVendor;
	}
	public void setProductVendor(String productVendor) {
		this.productVendor = productVendor;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public short getQuantityInStock() {
		return quantityInStock;
	}
	public void setQuantityInStock(short quantityInStock) {
		this.quantityInStock = quantityInStock;
	}
	public BigDecimal getBuyPrice() {
		return buyPrice;
	}
	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}
	public BigDecimal getMSRP() {
		return MSRP;
	}
	public void setMSRP(BigDecimal mSRP) {
		MSRP = mSRP;
	}

}
