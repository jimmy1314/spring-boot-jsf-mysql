package com.demo.springbootmysqljpajsf.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;


@Entity(name = "orderdetails")
public class Orderdetails {

    @EmbeddedId
    private OrderdetailsId orderdetailsId;

    @Column(name = "quantityOrdered")
    private Integer quantityOrdered;

    @Column(name = "priceEach")
    private BigDecimal priceEach;

    @Column(name = "orderLineNumber")
    private Short orderLineNumber;

    @ManyToOne
    @MapsId("orderNumber")
    @JoinColumn(name = "orderNumber", nullable = false)
    private Orders orders;

    @ManyToOne
    @MapsId("productCode")
    @JoinColumn(name = "productCode", nullable = false)
    private Products products;

    public OrderdetailsId getOrderdetailsId() {
        return orderdetailsId;
    }

    public void setOrderdetailsId(OrderdetailsId orderdetailsId) {
        this.orderdetailsId = orderdetailsId;
    }

    public Integer getQuantityOrdered() {
        return quantityOrdered;
    }

    public void setQuantityOrdered(Integer quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    public BigDecimal getPriceEach() {
        return priceEach;
    }

    public void setPriceEach(BigDecimal priceEach) {
        this.priceEach = priceEach;
    }

    public Short getOrderLineNumber() {
        return orderLineNumber;
    }

    public void setOrderLineNumber(Short orderLineNumber) {
        this.orderLineNumber = orderLineNumber;
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    
}
