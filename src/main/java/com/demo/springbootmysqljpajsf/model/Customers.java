package com.demo.springbootmysqljpajsf.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name = "customers")
public class Customers implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@NotNull
	//@Size(max = 11)
	@Column(name="customerNumber")
	private Integer customerNumber;
	@NotNull
	@Size(max = 50)
	@Column(name="customerName")
	private String customerName;
	@NotNull
	@Size(max = 50)
	@Column(name="contactLastName")
	private String contactLastName;
	@NotNull
	@Size(max = 50)
	@Column(name="contactFirstName")
	private String contactFirstName;
	@NotNull
	@Size(max = 50)
	@Column(name="phone")
	private String phone;
	@NotNull
	@Size(max = 50)
	@Column(name="addressLine1")
	private String addressLine1;
	@Size(max = 50)
	@Column(name="addressLine2")
	private String addressLine2;
	
	@NotNull
	@Size(max = 50)
	@Column(name="city")
	private String city;
	@Size(max = 50)
	@Column(name="state")
	private String state;
	@Size(max = 15)
	@Column(name="postalCode")
	private String postalCode;
	@NotNull
	@Size(max = 50)
	@Column(name="country")
	private String country;
	
	@Size(max = 11)
	@Column(name="salesRepEmployeeNumber")
	private Integer salesRepEmployeeNumber;
	@Column(name="creditLimit")
	private BigDecimal creditLimit;

	public Integer getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(Integer customerNumber) {
		this.customerNumber = customerNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getContactLastName() {
		return contactLastName;
	}
	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}
	public String getContactFirstName() {
		return contactFirstName;
	}
	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Integer getSalesRepEmployeeNumber() {
		return salesRepEmployeeNumber;
	}
	public void setSalesRepEmployeeNumber(Integer salesRepEmployeeNumber) {
		this.salesRepEmployeeNumber = salesRepEmployeeNumber;
	}
	public BigDecimal getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}
	
	

}
