package com.demo.springbootmysqljpajsf.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OrderdetailsId implements Serializable{

    private static final long serialVersionUID = 1L;

    @Column(name = "orderNumber")
    private Integer orderNumber;
    @Column(name = "productCode")
    private String productCode;

    public OrderdetailsId(){}
    
    public OrderdetailsId(Integer orderNumber, String productCode) {
        this.orderNumber = orderNumber;
        this.productCode = productCode;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

}
