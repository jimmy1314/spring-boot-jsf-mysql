package com.demo.springbootmysqljpajsf.service;

import java.util.List;

import com.demo.springbootmysqljpajsf.model.Orderdetails;
import com.demo.springbootmysqljpajsf.repository.OrderdetailsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderdetailsService {

    @Autowired
    private OrderdetailsRepository orderdetailsRepository;

    public List<Orderdetails> getAllOrderdetailsProducts(Integer orderNumber){
        List<Orderdetails> orderdetails = orderdetailsRepository.findByOrdersOrderNumber(orderNumber);
        for (Orderdetails orderdetails2 : orderdetails) {
            orderdetails2.getOrderdetailsId().getProductCode();
        }
        return orderdetails;
    }

    public List<Orderdetails> getAllOrderdetails(Integer orderNumber){
        return orderdetailsRepository.findByOrdersOrderNumber(orderNumber);
    }

    public List<Orderdetails> getAll(){
        return orderdetailsRepository.findAll();
    }

}
