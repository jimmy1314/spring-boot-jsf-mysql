package com.demo.springbootmysqljpajsf.service;

import org.springframework.stereotype.Service;

import com.demo.springbootmysqljpajsf.model.Customers;
import com.demo.springbootmysqljpajsf.repository.CustomersRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


@Service
public class CustomersService {

	@Autowired
	private CustomersRepository customersRepository;

	public List<Customers> getAll(){
		return customersRepository.findAll();
	}
}
