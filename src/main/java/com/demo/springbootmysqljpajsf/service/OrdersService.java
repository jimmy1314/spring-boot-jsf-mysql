package com.demo.springbootmysqljpajsf.service;

import org.springframework.stereotype.Service;

import com.demo.springbootmysqljpajsf.model.Orders;
import com.demo.springbootmysqljpajsf.repository.OrdersRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


@Service
public class OrdersService {

	@Autowired
	private OrdersRepository ordersRepository;
	
	public List<Orders> getAllOrders(Integer customerNumber){
		List<Orders> orders = ordersRepository.findByCustomersCustomerNumber(customerNumber);
		return orders;
	}

	public List<Orders> getAll(){
		return ordersRepository.findAll();
	}
}