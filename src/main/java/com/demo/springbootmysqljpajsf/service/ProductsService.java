package com.demo.springbootmysqljpajsf.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.springbootmysqljpajsf.model.Products;
import com.demo.springbootmysqljpajsf.repository.ProductsRepository;

@Service
public class ProductsService {

	@Autowired
	private ProductsRepository productsRepository;

	public Optional<Products> getAllProducts(String productCode) {
		return productsRepository.findById(productCode);
	}

	public List<Products> getAll(){
		return productsRepository.findAll();
	}
	
}
