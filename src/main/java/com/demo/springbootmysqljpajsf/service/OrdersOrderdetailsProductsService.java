package com.demo.springbootmysqljpajsf.service;

import java.util.ArrayList;
import java.util.List;

import com.demo.springbootmysqljpajsf.model.Orderdetails;
import com.demo.springbootmysqljpajsf.model.Orders;
import com.demo.springbootmysqljpajsf.model.OrdersOrderdetailsProducts;
import com.demo.springbootmysqljpajsf.model.Products;
import com.demo.springbootmysqljpajsf.repository.OrderdetailsRepository;
import com.demo.springbootmysqljpajsf.repository.OrdersRepository;
import com.demo.springbootmysqljpajsf.repository.ProductsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrdersOrderdetailsProductsService {

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private OrderdetailsRepository orderdetailsRepository;

    @Autowired
    private ProductsRepository productsRepository;

    public List<OrdersOrderdetailsProducts> getAllrdersOrderdetailsProducts(Integer customerNumber){
        //
        List<OrdersOrderdetailsProducts> orderdetailsProducts = new ArrayList<>();
        //get all orders by customerNumber
        List<Orders> orders = ordersRepository.findByCustomersCustomerNumber(customerNumber);
        for (Orders orders2 : orders) {
            Integer orderNumber= orders2.getOrderNumber();

            //get all orderdetails by orderNumber
            List<Orderdetails> orderdetails = orderdetailsRepository.findByOrdersOrderNumber(orderNumber);

            for (Orderdetails orderdetails2 : orderdetails) {
                String productCode = orderdetails2.getOrderdetailsId().getProductCode();
                
                //get products by productCode
                Products products = productsRepository.getOne(productCode);
                //new an OrdersOrderdetailsProducts
                OrdersOrderdetailsProducts selectedOrdersOrderdetailsProducts 
                = new OrdersOrderdetailsProducts(
                    orders2.getOrderNumber(),
                    orders2.getOrderDate(),
                    orders2.getRequiredDate(),
                    orders2.getShippedDate(),
                    orders2.getStatus(),
                    orders2.getComments(),

                    orderdetails2.getQuantityOrdered(),
                    orderdetails2.getPriceEach(),
                    orderdetails2.getOrderLineNumber(),

                    products.getProductCode(),
                    products.getProductName(),
                    products.getProductLine(),
                    products.getProductScale(),
                    products.getProductVendor(),
                    products.getProductDescription(),
                    products.getQuantityInStock(),
                    products.getBuyPrice(),
                    products.getMSRP()
                    );

                    // add to OrdersOrderdetailsProducts list
                    orderdetailsProducts.add(selectedOrdersOrderdetailsProducts);
            }
        }
        return orderdetailsProducts;
    }
}
