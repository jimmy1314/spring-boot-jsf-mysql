package com.demo.springbootmysqljpajsf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"com.demo.springbootmysqljpajsf"})
@EnableJpaRepositories(basePackages = {"com.demo.springbootmysqljpajsf.repository"})
@EntityScan(basePackages = {"com.demo.springbootmysqljpajsf.model"})
public class SpringBootMysqlJpaJsfApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMysqlJpaJsfApplication.class, args);
	}

}
