 package com.demo.springbootmysqljpajsf;


 import org.springframework.context.annotation.Configuration;
 import org.springframework.core.Ordered;
 import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
 import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
 @Configuration
 public class DiscoveryPageRedirect implements WebMvcConfigurer{

 	// redirect discovery page to home page
 	  @Override
 	   public void addViewControllers(ViewControllerRegistry registry) {
 	     registry.addViewController("/")
 	         .setViewName("forward:/discovery.xhtml");
 	     registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
 	   }
 }
