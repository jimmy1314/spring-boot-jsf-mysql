package com.demo.springbootmysqljpajsf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.demo.springbootmysqljpajsf.model.Orders;

@Repository
public interface OrdersRepository extends JpaRepository<Orders, Integer>{

    public List<Orders> findByCustomersCustomerNumber(Integer customerNumber);
}