package com.demo.springbootmysqljpajsf.repository;

import java.util.List;

import com.demo.springbootmysqljpajsf.model.Orderdetails;
import com.demo.springbootmysqljpajsf.model.OrderdetailsId;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderdetailsRepository extends JpaRepository<Orderdetails, OrderdetailsId> {

    public List<Orderdetails> findByOrdersOrderNumber(Integer orderNumber);

}
