package com.demo.springbootmysqljpajsf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.springbootmysqljpajsf.model.Customers;

@Repository
public interface CustomersRepository extends JpaRepository<Customers, Integer>{

}
