package com.demo.springbootmysqljpajsf.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.demo.springbootmysqljpajsf.model.Customers;
import com.demo.springbootmysqljpajsf.model.OrdersOrderdetailsProducts;
import com.demo.springbootmysqljpajsf.service.CustomersService;
import com.demo.springbootmysqljpajsf.service.OrdersOrderdetailsProductsService;


@Named("dtDiscoveryView")
@ViewScoped
public class DiscoveryView implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<Customers> customers;

    private List<OrdersOrderdetailsProducts> ordersOrderdetailsProducts;

    @Inject
    private CustomersService customersService;

    @Inject
    private OrdersOrderdetailsProductsService ordersOrderdetailsProductsService;

    @PostConstruct
    public void init() {
        customers = customersService.getAll();

        ordersOrderdetailsProducts = new ArrayList<>();
    }

    public List<Customers> getCustomers() {
        return customers;
    }

    public List<OrdersOrderdetailsProducts> getOrdersOrderdetailsProducts() {
        return ordersOrderdetailsProducts;
    }
    
    public void selectCustomer(Customers c) {
        ordersOrderdetailsProducts=ordersOrderdetailsProductsService.getAllrdersOrderdetailsProducts(c.getCustomerNumber());
    }

    public void setOrdersOrderdetailsProducts(List<OrdersOrderdetailsProducts> ordersOrderdetailsProducts) {
        this.ordersOrderdetailsProducts = ordersOrderdetailsProducts;
    }


}
