// package com.demo.springbootmysqljpajsf.controller;

// import java.util.List;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.web.bind.annotation.PathVariable;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RestController;

// import com.demo.springbootmysqljpajsf.model.Customers;
// import com.demo.springbootmysqljpajsf.model.Orders;
// import com.demo.springbootmysqljpajsf.model.Products;
// import com.demo.springbootmysqljpajsf.repository.OrdersRepository;
// import com.demo.springbootmysqljpajsf.repository.ProductsRepository;
// import com.demo.springbootmysqljpajsf.service.CustomersService;
// // import com.demo.springbootmysqljpajsf.service.OrdersService;

// @RestController
// public class APIController {

// 	@Autowired
// 	private CustomersService customerService;
// 	@Autowired
// 	private ProductsRepository productsRepository;
// 	// @Autowired
// 	// private OrdersRepository ordersRepository;

// 	// @Autowired
// 	// private OrdersService ordersService;
	
// 	@RequestMapping("customers")
// 	public List<Customers> getAllCustomers() {
// 		return customerService.getAllCustomers();
// 	}
	
// 	@RequestMapping("products")
// 	public List<Products> getAllProducts() {
// 		return productsRepository.findAll();
// 	}

// 	//testing url http://localhost:8080/orders/363
// 	// @RequestMapping("orders/{customerNumber}")
// 	// public List<Orders> getAllOrdersByCustomerNumber(@PathVariable Integer customerNumber) {
// 	// 	return ordersService.getAllOrders(customerNumber);
// 	// }

// 	// @RequestMapping("orders")
// 	// public List<Orders> getAllOrders() {
// 	// 	return ordersRepository.findAll();
// 	// }
	
// }
